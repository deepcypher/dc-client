# @Author: GeorgeRaven <archer>
# @Date:   2021-05-16T21:48:30+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-16T22:10:28+01:00
# @License: please see LICENSE file in project root

class DC():
    """DeepCypher API-Client Library/ Abstraction Class."""

    def __init__(self):
        """Initialise deepcypher abstraction object."""
        pass

    def connect(self):
        """Get a connection handler to the backend processing server."""
        pass

    def encrypt(self):
        """Encrypt given data fully homomorphically."""
        pass

    def decrypt(self):
        """Decrypt given cyphertext."""
        pass

    def train(self):
        """Train on given data using given model."""
        pass

    def infer(self):
        """Infer on given data using given model."""
        pass
